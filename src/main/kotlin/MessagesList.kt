class MessagesList : StringList {
    override var textList: MutableList<String> = mutableListOf()

    override fun newMessage(text : String){
        textList.add(text)
    }
    override fun getAllMessages():MutableList<String>{
        return textList.asReversed()
    }
}