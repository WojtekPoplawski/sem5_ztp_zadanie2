import decorators.*

fun main(args: Array<String>) {
    var messagesServer = AddMessageIndexDecorator(
        AddSendTimeDecorator(
            AddReciveTimeDecorator(
                FilterMessagesDecorator(CensorMessagesDecorator(MessagesList(), "C#"), "Java")
            )
        )
    )

    messagesServer.newMessage("Jadę na ryby")
    messagesServer.newMessage("Java jest gorsza od Kotlina")
    messagesServer.newMessage("A o C# nawet nie wspomnę")
    messagesServer.newMessage("No JavaScript czasami tylko dorównuje, ale jest lepsza od C#/C++")
    messagesServer.newMessage("A co z TypeScriptem")
    println(messagesServer.getAllMessages())

    var evenMoreCensoredMessageServer = CensorMessagesDecorator(messagesServer, "TypeScript")
    println(evenMoreCensoredMessageServer.getAllMessages())

    var encodedMessageServer = EncodeMessagesDecorator(MessagesList())

    encodedMessageServer.newMessage("Jadę na ryby")
    encodedMessageServer.newMessage("Java jest gorsza od Kotlina")
    encodedMessageServer.newMessage("A o C# nawet nie wspomnę")
    encodedMessageServer.newMessage("No JavaScript czasami tylko dorównuje, ale jest lepsza od C#/C++")
    encodedMessageServer.newMessage("A co z TypeScriptem")
    println(encodedMessageServer.getAllEncodedMessages())
    println(encodedMessageServer.getAllMessages())
}