package decorators

import StringList

class AddMessageIndexDecorator(stringList: StringList) : MessagesListDecorator(stringList) {
    override var textList: MutableList<String> = stringList.textList

    override fun newMessage(text: String) {
        super.newMessage("${textList.size + 1}. " + text)
    }
}