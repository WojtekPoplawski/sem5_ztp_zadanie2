package decorators

import StringList
import java.time.LocalDateTime

class AddSendTimeDecorator(stringList: StringList) : MessagesListDecorator(stringList) {
    override var textList: MutableList<String> = stringList.textList

    override fun newMessage(text: String) {
        super.newMessage(text+" / Wysłano : ${LocalDateTime.now()}")
    }

    override fun getAllMessages(): MutableList<String> {
        return super.getAllMessages()
    }
}