package decorators

import StringList
import java.time.LocalDateTime

class AddReciveTimeDecorator(stringList: StringList): MessagesListDecorator(stringList)  {
    override var textList: MutableList<String> = stringList.textList

    override fun newMessage(text: String) {
        super.newMessage(text+" / Odebrano : ${LocalDateTime.now()}")
    }

}