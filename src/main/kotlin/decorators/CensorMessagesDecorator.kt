package decorators

import StringList

class CensorMessagesDecorator (stringList: StringList, censorText: String) : MessagesListDecorator(stringList) {
    override var textList: MutableList<String> = stringList.textList
    var filter: String = censorText

    override fun getAllMessages(): MutableList<String> {
        return textList.map { element -> element.replace(filter,"***") }.toMutableList()
    }
}