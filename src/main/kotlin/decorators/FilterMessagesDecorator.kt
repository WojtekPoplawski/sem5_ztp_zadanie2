package decorators

import StringList

class FilterMessagesDecorator(stringList: StringList, filterText: String) : MessagesListDecorator(stringList) {
    override var textList: MutableList<String> = stringList.textList
    var filter: String = filterText

    override fun getAllMessages(): MutableList<String> {
        return super.getAllMessages().filter { element -> !element.contains(filter) }.toMutableList()
    }
}