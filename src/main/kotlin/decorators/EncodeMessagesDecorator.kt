package decorators

import StringList

class EncodeMessagesDecorator(stringList: StringList) : MessagesListDecorator(stringList) {
    override var textList: MutableList<String> = stringList.textList

    var encodedTextList: MutableList<ByteArray> = stringList.textList.map { element -> element.toByteArray(charSet) }.toMutableList()
    var charSet = Charsets.UTF_8

    override fun newMessage(text: String) {
        super.newMessage(text)
        encodedTextList.add(text.toByteArray(charSet))
    }

    override fun getAllMessages(): MutableList<String> {
        return encodedTextList.map { element -> element.toString(charSet) }.toMutableList()
    }
    fun getAllEncodedMessages(): MutableList<String> {
        return encodedTextList.map { element -> element.toString() }.toMutableList()
    }
}