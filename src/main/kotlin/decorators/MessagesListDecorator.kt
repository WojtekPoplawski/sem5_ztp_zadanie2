package decorators

import StringList

abstract class MessagesListDecorator(private val stringList: StringList) : StringList {

    override fun newMessage(text: String) {
        return stringList.newMessage(text)
    }

    override fun getAllMessages(): MutableList<String> {
        return stringList.getAllMessages()
    }
}