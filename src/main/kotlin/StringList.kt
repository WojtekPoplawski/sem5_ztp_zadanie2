interface StringList {
    var textList: MutableList<String>

    fun newMessage(text : String)
    fun getAllMessages():MutableList<String>
}